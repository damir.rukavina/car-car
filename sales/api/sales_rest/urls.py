from django.urls import path

from .views import (
    api_salespeople,
    api_customers,
    api_sales,
    api_sale,
    api_automobiles,
    api_salesperson_history,
    api_automobile,
    )

urlpatterns = [
    path("salespeople/", api_salespeople, name="api_salespeople"),
    path("customers/", api_customers, name="api_customers"),
    path("sales/", api_sales, name="api_sales"),
    path("sale/<int:pk>/", api_sale, name="api_sale"),
    path("automobiles/", api_automobiles, name="api_automobiles"),
    path("automobile/<int:pk>/", api_automobile, name="api_automobile"),
    path("salesperson-history/<int:salesperson_id>/", api_salesperson_history, name="api_salesperson_history"),
]
