from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    manufacturer = models.CharField(max_length=50, null=True)
    model = models.CharField(max_length=50, null=True)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    availability = models.BooleanField(default=True)
    import_href = models.CharField(max_length=200, unique=True)

    # def get_api_url(self):
    #     return reverse("api_automobile", kwargs={"vin": self.vin})

    # def __str__(self):
    #     return self.vin


class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)

    def get_api_url(self):
        return reverse("api_customers", kwargs={"pk": self.id})

    def __str__(self):
        return self.name


class Sale(models.Model):
    sale_price = models.CharField(max_length=14)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesperson",
        on_delete=models.CASCADE
    )

    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE
    )
