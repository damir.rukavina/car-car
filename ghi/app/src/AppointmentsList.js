import { NavLink } from 'react-router-dom';
import React, { useEffect, useState } from 'react';


function displayTime(timeString) {
  const time = timeString.split(":");
  if (time[0] > 11) {
    return (time[0]-12) + ":" + time[1] + " PM";
  } else if (time[0] < 10) {
    return time[0][1] + ":" + time[1] + " AM";
  } else {
    return time[0] + ":" + time[1] + " AM";
  }
}

function AppointmentsList({appointments, fetchAppointments}) {
    if (appointments === undefined) {
        return null
    }

    const deleteAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointment/${appointment.id}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            fetchAppointments();
        }

    }

    const completeAppointment = async (appointment) => {
      const appointmentUrl = `http://localhost:8080/api/appointment/${appointment.id}/`;
      const fetchConfig = {
          method: "put",
          headers: {
              'Content-Type': 'application/json',
          }
      };
      const response = await fetch(appointmentUrl, fetchConfig);
      if (response.ok) {
          fetchAppointments();
      }

  }

    return (
        <>
        <p></p>
        <h1>Service Appointments</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/technicians">Technicians</NavLink>
        </p>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/appointments/new">Create appointment</NavLink>
        </p>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/appointments/history">Service history</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>VIP?</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {appointments.map(appointment => {
              return (
                <tr key={appointment.id}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.customer_name }</td>
                  <td>{ appointment.is_VIP }</td>
                  <td>{new Date(appointment.date).toLocaleDateString() }</td>
                  <td>{ displayTime(appointment.time) }</td>
                  <td>{ appointment.technician.name }</td>
                  <td>{ appointment.reason }</td>
                  <td>
                    <button className="btn btn-outline-danger" onClick={() => deleteAppointment(appointment)}>Cancel</button>
                  </td>
                  <td>
                    <button className="btn btn-outline-success" onClick={() => completeAppointment(appointment)}>Complete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default AppointmentsList;
