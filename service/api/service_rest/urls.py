from django.urls import path

from .views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_appointment,
    api_service_history,
    )

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technician/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointment/<int:pk>/", api_appointment, name="api_appointment"),
    path("appointments/history/", api_service_history, name="api_service_history"),
]
