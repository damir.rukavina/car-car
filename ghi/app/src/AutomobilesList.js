import { NavLink } from 'react-router-dom';

function AutomobilesList({automobiles, fetchAutomobiles}) {
    if (automobiles === undefined) {
        return null
    }
    fetchAutomobiles();

    return (
        <>
        <p></p>
        <h1>Automobiles</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/automobiles/new">New automobile</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Year</th>
              <th>Color</th>
              <th>VIN</th>
            </tr>
          </thead>
          <tbody>
            {automobiles.map(automobile => {
              return (
                <tr key={automobile.id}>
                  <td>{ automobile.model.manufacturer.name }</td>
                  <td>{ automobile.model.name }</td>
                  <td>{ automobile.year }</td>
                  <td>{ automobile.color }</td>
                  <td>{ automobile.vin }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default AutomobilesList;
