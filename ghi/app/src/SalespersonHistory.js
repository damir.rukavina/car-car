import React, {useEffect, useState} from 'react';

function SalespersonHistory() {

    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespersons, setSalespersons] = useState([]);

    const handleSalespersonChange = async (event) => {
        const value = event.target.value
        setSalesperson(value)

        const url = `http://localhost:8090/api/salesperson-history/${value}/`;
        const response = await fetch(url);
        const data = await response.json();
        const salesData = data.sales
        setSales(salesData);
    }

    const fetchSalespersonData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespeople)
        }
    }

    useEffect(() => {
        fetchSalespersonData();
    }, []);

    return (
        <>
        <p></p>
        <h1>Salesperson History</h1>
        <p></p>
        <div className="mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                  <option value="">Choose a salesperson</option>
                  {salespersons.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.name}
                        </option>
                    );
                  })}
                </select>
              </div>
        <table className="table table-striped table-hover align-middle mt-5">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sale price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.name }</td>
                            <td>{ sale.customer.name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>{ sale.sale_price }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )

}

export default SalespersonHistory;
