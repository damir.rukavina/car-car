import { NavLink } from 'react-router-dom';


function CustomerList({customers, fetchCustomers}) {
    if (customers === undefined) {
        return null
    }
    fetchCustomers();

    return (
        <>
        <p></p>
        <h1>Customers</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/customers/new">New customer</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Address</th>
              <th>Phone number</th>
            </tr>
          </thead>
          <tbody>
            {customers.map(customer => {
              return (
                <tr key={customer.id}>
                  <td>{ customer.name }</td>
                  <td>{ customer.address }</td>
                  <td>{ customer.phone_number }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default CustomerList;
