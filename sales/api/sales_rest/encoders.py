from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "manufacturer",
        "model",
        "vin",
        "availability",
        "id",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "sale_price",
        "automobile",
        "salesperson",
        "customer",
    ]
    encoders = {
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }
