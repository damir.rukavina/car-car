import { NavLink } from 'react-router-dom';


function ManufacturersList( {manufacturers, fetchManufacturers} ) {
    if (manufacturers === undefined) {
        return null
    }
    fetchManufacturers();

    return (
        <>
        <p></p>
        <h1>Manufacturers</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/manufacturers/new">New manufacturer</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map(manufacturer => {
              return (
                <tr key={manufacturer.id}>
                  <td>{ manufacturer.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default ManufacturersList;
