import { NavLink } from 'react-router-dom';
import { useEffect, useState } from 'react';


function TechniciansList() {

    const [techs, setTechs] = useState([]);

    const fetchTechs = async () => {
      const techsUrl = 'http://localhost:8080/api/technicians/'
      const response = await fetch(techsUrl);

      if (response.ok) {
        const data = await response.json();
        const techs = data.technicians;
        setTechs(techs);
      }
    }

    useEffect(() => {
      fetchTechs();
    }, [])

    if (techs === undefined) {
      return null
  }

    return (
        <>
        <p></p>
        <h1>Technicians</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/technicians/new">New technician</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Employee number</th>
            </tr>
          </thead>
          <tbody>
            {techs.map(technician => {
              return (
                <tr key={technician.id}>
                  <td>{ technician.name }</td>
                  <td>{ technician.employee_number }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default TechniciansList;
