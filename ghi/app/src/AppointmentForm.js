import React, {useEffect, useState} from 'react';

function AppointmentForm ({fetchAppointments}) {

    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
  }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer_name = customer;
        data.date = date;
        data.time = time;
        data.reason = reason;
        data.technician_id = technician;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setReason('');
            setTechnician('');
            fetchAppointments();
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer_name" className="form-control"/>
                <label htmlFor="customer_name">Customer name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={date} placeholder="Appointment date" required type="date" name="datetime-local" id="scheduled_appointment" className="form-control"/>
                <label htmlFor="scheduled_appointment">Appointment date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTimeChange} value={time} placeholder="Appointment time" required type="time" name="datetime-local" id="scheduled_appointment" className="form-control"/>
                <label htmlFor="scheduled_appointment">Appointment time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason_for_appointment" className="form-control"/>
                <label htmlFor="reason_for_appointment">Reason for appointment</label>
              </div>
              <div className="mb-3">
                <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                  <option value="">Choose a technician</option>
                  {technicians.map(technician => {
                    return (
                        <option key={technician.id} value={technician.id}>
                            {technician.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default AppointmentForm;
