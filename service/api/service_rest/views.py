from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianEncoder, ServiceAppointmentEncoder
from .models import AutomobileVO, Technician, ServiceAppointment


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_technician(request, pk):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.filter(completed=False)
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            technician_id = content["technician_id"]
            technician = Technician.objects.get(pk=technician_id)
            content["technician"] = technician

            automobiles = AutomobileVO.objects.values()
            inventory_vins = []
            for automobile in automobiles:
                inventory_vins.append(automobile["vin"])
            if content["vin"] in inventory_vins:
                content["is_VIP"] = "Yes"

            appointment = ServiceAppointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "PUT"])
def api_appointment(request, pk):
    if request.method == "DELETE":
        try:
            appointment = ServiceAppointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            appointment = ServiceAppointment.objects.get(id=pk)
            setattr(appointment, "completed", True)
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_service_history(request):
    if request.method == "GET":
        try:
            appointments = ServiceAppointment.objects.all()
            return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder
        )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


# @require_http_methods(["GET"])
# def api_service_history(request, vin):
#     if request.method == "GET":
#         try:
#             appointments = ServiceAppointment.objects.filter(vin=vin)
#             return JsonResponse(
#             {"appointments": appointments},
#             encoder=ServiceAppointmentEncoder
#         )
#         except ServiceAppointment.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
