import React, {useEffect, useState} from 'react';

function SaleForm(props) {

    const [salePrice, setSalePrice] = useState('$');
    const [automobile, setAutomobile] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespersons, setSalespersons] = useState([]);
    const [customer, setCustomer] = useState('');
    const [customers, setCustomers] = useState([]);

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setSalePrice(value);
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.sale_price = salePrice;
        data.automobile_id = automobile;
        data.salesperson_id = salesperson;
        data.customer_id = customer;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale)

            setSalePrice('$');
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            props.fetchSales();
        }
    }

    const fetchAutomobileData = async () => {
        const url = 'http://localhost:8090/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.automobiles)
        }
    }

    const fetchSalespersonData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespeople)
        }
    }

    const fetchCustomerData = async () => {
        const url = 'http://localhost:8090/api/customers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchAutomobileData();
        fetchSalespersonData();
        fetchCustomerData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={salePrice} placeholder="Sale Price" required type="text" name="salePrice" id="salePrice" className="form-control"/>
                <label htmlFor="vin">Sale price</label>
              </div>
              <div className="mb-3">
                <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                  <option value="">Choose an automobile</option>
                  {automobiles.map(automobile => {
                    return (
                        <option key={automobile.id} value={automobile.id}>
                            {automobile.year} {automobile.manufacturer} {automobile.model}, {automobile.color} (VIN: {automobile.vin})
                        </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                  <option value="">Choose a salesperson</option>
                  {salespersons.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>
                            {customer.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default SaleForm;
