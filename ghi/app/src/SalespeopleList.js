import { NavLink } from 'react-router-dom';


function SalespersonList({salespeople}) {
    if (salespeople === undefined) {
        return null
    }

    return (
        <>
        <p></p>
        <h1>Salespeople</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/salespeople/new">New salesperson</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Employee number</th>
            </tr>
          </thead>
          <tbody>
            {salespeople.map(salesperson => {
              return (
                <tr key={salesperson.id}>
                  <td>{ salesperson.name }</td>
                  <td>{ salesperson.employee_number }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default SalespersonList;
