import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from "react";

import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentsList from './AppointmentsList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturersList from './ManufacturersList';
import AppointmentForm from './AppointmentForm';
import ModelsList from './ModelsList';
import AutomobilesList from './AutomobilesList';
import Inventory from './Inventory';
import ModelForm from './ModelForm';
import AutomobileForm from './AutomobileForm';
import TechniciansList from './TechniciansList';
import TechnicianForm from './TechnicianForm';
import ServiceHistory from './ServiceHistory';
import SalesList from './SalesList';
import SaleForm from './SaleForm';
import SalespersonList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import SalespersonHistory from './SalespersonHistory';
import CustomerList from './CustomersList';
import CustomerForm from './CustomerForm';


function App(props) {
  const [appointments, setAppointments] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);


  const fetchAppointments = async () => {
    const appointmentsUrl = 'http://localhost:8080/api/appointments/'
    const response = await fetch(appointmentsUrl);

    if (response.ok) {
      const data = await response.json();
      const appointments = data.appointments;
      setAppointments(appointments);
    }
  }

  const fetchManufacturers = async () => {
    const manufacturersUrl = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(manufacturersUrl);

    if (response.ok) {
      const data = await response.json();
      const manufacturers = data.manufacturers;
      setManufacturers(manufacturers);
    }
  }

  const fetchModels = async () => {
    const modelsUrl = 'http://localhost:8100/api/models/'
    const response = await fetch(modelsUrl);

    if (response.ok) {
      const data = await response.json();
      const models = data.models;
      setModels(models);
    }
  }

  const fetchAutomobiles = async () => {
    const automobilesUrl = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(automobilesUrl);

    if (response.ok) {
      const data = await response.json();
      const automobiles = data.autos;
      setAutomobiles(automobiles);
    }
  }

  const fetchTechnicians = async () => {
    const techniciansUrl = 'http://localhost:8080/api/technicians/'
    const response = await fetch(techniciansUrl);

    if (response.ok) {
      const data = await response.json();
      const technicians = data.technicians;
      setTechnicians(technicians);
    }
  }

  const fetchSales = async () => {
    const salesUrl = 'http://localhost:8090/api/sales/'
    const response = await fetch(salesUrl);

    if (response.ok) {
      const data = await response.json();
      const sales = data.sales;
      setSales(sales);
    }
  }

  const fetchSalespeople = async () => {
    const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(salespeopleUrl);

    if (response.ok) {
      const data = await response.json();
      const salespeople = data.salespeople;
      setSalespeople(salespeople);
    }
  }

  const fetchCustomers = async () => {
    const customersUrl = 'http://localhost:8090/api/customers/'
    const response = await fetch(customersUrl);

    if (response.ok) {
      const data = await response.json();
      const customers = data.customers;
      setCustomers(customers);
    }
  }

  useEffect(() => {
    fetchAppointments();
    fetchManufacturers();
    fetchModels();
    fetchAutomobiles();
    fetchTechnicians();
    fetchSales();
    fetchSalespeople();
    fetchCustomers();
  }, [])


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage/>} />
          <Route path="/inventory" element={<Inventory/>}></Route>
          <Route path="/manufacturers">
            <Route index element={<ManufacturersList manufacturers={manufacturers} fetchManufacturers={fetchManufacturers}/>} />
            <Route path="new" element={<ManufacturerForm/>} />
          </Route>
          <Route path="/models">
            <Route index element={<ModelsList models={models} fetchModels={fetchModels}/>} />
            <Route path="new" element={<ModelForm fetchModels={fetchModels}/>} />
          </Route>
          <Route path="/automobiles">
            <Route index element={<AutomobilesList automobiles={automobiles} fetchAutomobiles={fetchAutomobiles}/>} />
            <Route path="new" element={<AutomobileForm/>} />
          </Route>
          <Route path="/technicians">
            <Route index element={<TechniciansList technicians={technicians} fetchTechicians={fetchTechnicians}/>} />
            <Route path="new" element={<TechnicianForm/>} />
          </Route>
          <Route path="/salespeople">
            <Route index element={<SalespersonList salespeople={salespeople} fetchSalespeople={fetchSalespeople}/>} />
            <Route path="new" element={<SalespersonForm/>} />
          </Route>
          <Route path="/customers">
            <Route index element={<CustomerList customers={customers} fetchCustomers={fetchCustomers}/>} />
            <Route path="new" element={<CustomerForm/>} />
          </Route>
          <Route path="/appointments">
            <Route index element={<AppointmentsList appointments={appointments} fetchAppointments={fetchAppointments}/>} />
            <Route path="new" element={<AppointmentForm fetchAppointments={fetchAppointments}/>} />
            <Route path="history" element={<ServiceHistory/>} />
          </Route>
          <Route path="/sales">
            <Route index element={<SalesList sales={sales} fetchSales={fetchSales}/>} />
            <Route path="new" element={<SaleForm fetchSales={fetchSales}/>} />
            <Route path="salesperson-history" element={<SalespersonHistory/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
