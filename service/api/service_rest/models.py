from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    # def get_api_url(self):
    #     return reverse("api_technician", kwargs={"pk": self.id})

    # def __str__(self):
    #     return self.name


class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    date = models.DateTimeField()
    time = models.TimeField()
    reason = models.CharField(max_length=300)
    is_VIP = models.CharField(max_length=3, null=True)
    completed = models.BooleanField(default=False, null=True, blank=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE
    )

    def complete_appointment(self):
        self.completed = True
        self.save()

    # def get_api_url(self):
    #     return reverse("api_appointment", kwargs={"pk": self.id})
