import { NavLink } from 'react-router-dom';

function Inventory() {
    return (
        <>
        <p></p>
        <h1>Inventory</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/manufacturers">Manufacturers</NavLink>
        </p>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/models">Vehicle models</NavLink>
        </p>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/automobiles">Automobiles</NavLink>
        </p>
        </>
      );
    }

export default Inventory;
