from common.json import ModelEncoder
from .models import Technician, ServiceAppointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]

# class ServiceAppointmentsEncoder(ModelEncoder):
#     model = ServiceAppointment,
#     properties = [
#         "id",
#         "vin",
#         "customer_name",
#         "date",
#         "time",
#         "reason",
#         "is_VIP",
#         "completed",
#         "technician",
#     ]
#     encoders = {
#         "technician": TechnicianEncoder(),
#     }

class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment,
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "time",
        "reason",
        "is_VIP",
        "completed",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
