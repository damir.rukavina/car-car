import React, {useEffect, useState} from 'react';

function displayTime(timeString) {
    const time = timeString.split(":");
    if (time[0] > 11) {
      return (time[0]-12) + ":" + time[1] + " PM";
    } else if (time[0] < 10) {
      return time[0][1] + ":" + time[1] + " AM";
    } else {
      return time[0] + ":" + time[1] + " AM";
    }
  }

function ServiceHistory() {

    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState('');

    const handleSearch = async (event) => {
        const value = event.target.value
        setSearch(value)
    }

    async function fetchAppointments() {
        const url = 'http://localhost:8080/api/appointments/history/';
        const response = await fetch(url);
        const data = await response.json();
        const appointmentData = data.appointments
        const result = appointmentData.filter(appointment => appointment.vin === search)
        setAppointments(result);

    }

    useEffect(() => {
        fetchAppointments();
    }, []);

    return (
        <>
        <p></p>
        <h1>Service Appointment History</h1>
        <p></p>
        <div className="input-group">
            <input onChange={handleSearch} value={search} placeholder="Enter VIN here" type="search" id="form1" className="form-control" />
            <button type="button" onClick={fetchAppointments} className="btn btn-primary">
                Search
            </button>
        </div>
        <table className="table table-striped table-hover align-middle mt-5">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.customer_name }</td>
                            <td>{new Date(appointment.date).toLocaleDateString() }</td>
                            <td>{ displayTime(appointment.time) }</td>
                            <td>{ appointment.technician.name }</td>
                            <td>{ appointment.reason }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )

}

export default ServiceHistory;
