import { NavLink } from 'react-router-dom';
import React, { useEffect, useState } from 'react';


function SalesList({sales, fetchSales}) {
    if (sales === undefined) {
        return null
    }

    return (
        <>
        <p></p>
        <h1>Sales</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/sales/new">New sale</NavLink>
        </p>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/salespeople">Salespeople</NavLink>
        </p>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/sales/salesperson-history">Salesperson history</NavLink>
        </p>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/customers">Customers</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Employee #</th>
              <th>Customer Name</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {sales.map(sale => {
              return (
                <tr key={sale.id}>
                  <td>{ sale.salesperson.name }</td>
                  <td>{ sale.salesperson.employee_number }</td>
                  <td>{ sale.customer.name }</td>
                  <td>{ sale.automobile.vin }</td>
                  <td>{ sale.sale_price }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default SalesList;
