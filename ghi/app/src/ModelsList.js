import { NavLink } from 'react-router-dom';


function ModelsList( {models, fetchModels} ) {
    if (models === undefined) {
        return null
    }
    fetchModels();

    return (
        <>
        <p></p>
        <h1>Vehicle models</h1>
        <p>
            <NavLink className="navbar-brand" aria-current="page" to="/models/new">New model</NavLink>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map(model => {
              return (
                <tr key={model.id}>
                  <td>{ model.manufacturer.name }</td>
                  <td>{ model.name }</td>
                  <td><img src={ model.picture_url } className="img-thumbnail shoes" width="200"></img></td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default ModelsList;
